package com.ochedowska.aneta.latajacykrecik;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * Created by Aneta on 2016-01-14.
 */
public class MainThread extends Thread {
    private int FPS = 30;
    private double avaragefPS;
    private SurfaceHolder surfaceHolder;
    private GamePanel gamePanel;
    private boolean running;
    public static Canvas canvas;

    public MainThread(SurfaceHolder surfaceHolder, GamePanel gamePanel) {
        //wywoluje konstruktor klasy Thread
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel = gamePanel;
    }

    //znow musze nadpisac metody klasy z ktorej dziedzicze
    @Override
    public void run() {
        long startTime;
        long timeMillis;
        long waitTime;
        long totalTime = 0;
        int frameCout = 0;
        //czas petli w milisekundach
        long targetTime = 1000 / FPS;

        while (running) {
            startTime = System.nanoTime();
            canvas = null;

            //proba zaalokowania tła
            try {
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder) {
                    this.gamePanel.update();
                    this.gamePanel.draw(canvas);
                }
            } catch (Exception e) {
            } finally {
                if (canvas != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            //chce wiedziec ile trwalo w milisekundach
            //jedno przjscie watku z odświerzeniem i odmalowaniem tla
            timeMillis = (System.nanoTime() - startTime) / 1000000;
            //celem jest zeby wszystko trwalo dokladnie targetTime
            waitTime = targetTime - timeMillis;

            try {
                this.sleep(waitTime);
            } catch (Exception e) {

            }
            totalTime += System.nanoTime() - startTime;
            frameCout++;
            //jezeli udalo nam sie przejsc wszystko odswierzenie przemalowanie 30 razy
            if (frameCout == FPS) {
                avaragefPS = 1000 / ((totalTime / frameCout) / 1000000);
                frameCout = 0;
                totalTime = 0;
                System.out.println(avaragefPS);
            }
        }
    }

    public void setRunnig(boolean b) {
        running = b;
    }

}

