package com.ochedowska.aneta.latajacykrecik;

import android.graphics.Bitmap;
import android.graphics.Canvas;


/**
 * Created by Aneta on 2016-01-15.
 */
public class Player extends GameObject{
    private Bitmap spritesheet;

    //wazne score naliczany w klasie uzytkownika
    private int score;

    private boolean up;
    private boolean playing;
    private Animation animation = new Animation();
    private long startTime;

    public Player(Bitmap res, int w, int h, int numFrames) {

        x = 100;
        y = GamePanel.HEIGHT / 2;
        dy = 0;
        score = 0;
        height = h;
        width = w;

        Bitmap[] image = new Bitmap[numFrames];

        spritesheet = res;

        for (int i = 0; i < image.length; i++)
        {
            image[i] = Bitmap.createBitmap(spritesheet, i*width, 0, width, height);
        }
        animation.setFrames(image);
        //opoznienie
        animation.setDelay(0);
        startTime = System.nanoTime();
    }

    public void setUp(boolean b){up = b;}

    public void update()
    {
        //upłynelo
        long elapsed = (System.nanoTime()-startTime)/1000000;
        if(elapsed>100)
        {
            score++;
            startTime = System.nanoTime();
        }
        animation.update();

        if(up){
            dy -=2;
        }
        else{
            dy +=2;
        }

        if(dy>17)dy = 17;
        if(dy<-17)dy = -17;

        y += dy*2;
        dy = 0;
    }

    public void draw(Canvas canvas)
    {
        canvas.drawBitmap(animation.getImage(),x,y,null);
    }

    public int getScore(){return score;}
    public boolean getPlaying(){return playing;}
    public void setPlaying(boolean b){playing = b;}
    public void resetDY(){dy = 0;}
    public void resetScore(){score = 0;}
}
