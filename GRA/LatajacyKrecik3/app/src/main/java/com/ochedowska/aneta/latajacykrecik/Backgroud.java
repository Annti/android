package com.ochedowska.aneta.latajacykrecik;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by Aneta on 2016-01-15.
 */
public class Backgroud {
    private Bitmap image;
    private int x, y, dx;

    public Backgroud(Bitmap res) {
        image = res;
        dx = GamePanel.MOVESPEED;
    }

    public void update() {
        x += dx;
        //kiedy obraz wyszedl calkowicie poza ekran
        if (x < -GamePanel.WIDTH) {
            x = 0;
        }
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(image, x, y, null);
        //rysowanie obrazu kiedy sie przesunał
        //jezeli kawalek obrazu wyszed poza ekran
        if (x < 0) {
            //rysuje drugi obraz zaraz za nim
            canvas.drawBitmap(image, x + GamePanel.WIDTH, y, null);
        }
    }


}
