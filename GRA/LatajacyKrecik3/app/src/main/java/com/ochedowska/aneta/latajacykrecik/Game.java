package com.ochedowska.aneta.latajacykrecik;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

//na poczatek usowam ActionBar
public class Game extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //wylaczenie widocznosci tytulu
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //ustawienie aplikacji na fullscrean
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //odwoluje sie do nowego activity ( wczesniej oddwollywalam sie do domyslnego activiti R utworzonego podczas tworzenia projektu)
        //this to context calej klasy Game
        setContentView(new GamePanel(this));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //dodaje elemanty do paska akcji jezeli ejst obecny
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
