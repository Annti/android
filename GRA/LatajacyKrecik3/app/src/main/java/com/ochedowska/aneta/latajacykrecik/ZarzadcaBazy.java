package com.ochedowska.aneta.latajacykrecik;

/**
 * Created by Aneta on 2016-02-24.
 */


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

public class ZarzadcaBazy extends SQLiteOpenHelper {
    public ZarzadcaBazy(Context context) {
        super(context, "bestscores.db", (CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table bests(nr integer primary key autoincrement,best text);");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void addScore(String best) {
        //uchwyt do bazy
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues wartosci = new ContentValues();
        wartosci.put("best", best);
        db.insertOrThrow("bests", (String) null, wartosci);
        Log.d("dodany","rekord");
    }

//    public Cursor dajWszystkie() {
//        String[] kolumny = new String[]{"nr", "best"};
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor kursor = db.query("bests", kolumny, (String)null, (String[])null, (String)null, (String)null, (String)null);
//        Log.d("zainicjalizowany kursor","rekord");
//        return kursor;
//    }

    public List<Score> theBest() {
        LinkedList score = new LinkedList();
        String[] var10000 = new String[]{"nr", "best"};
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor kursor = db.rawQuery("select * from bests order by best desc limit 1", (String[])null);

        while(kursor.moveToNext()) {
            Score kontakt = new Score();
            kontakt.setBest(kursor.getString(1));
            score.add(kontakt);
        }

        return score;
    }
}