package com.ochedowska.aneta.latajacykrecik;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;

import java.util.Random;

/**
 * Created by Aneta on 2016-02-16.
 */
public class Missile extends GameObject {
    private int score;
    private int speed;
    //aby wygenerowac szybkosc bede uzywac rand
    private Random rand= new Random();
    private Animation animation = new Animation();
    private Bitmap spritesheet;
    public Missile(Bitmap res, int x, int y, int w, int h, int s, int numFrames) {
        //tutaj mozna zarowno wstawic super jak this nie ma znaczenia logika sie nie zmieni
        //super woła x z klasy GameObject
        super.x = x;
        super.y = y;

        width = w;
        height = h;
        score = s;
        //szybkosc to 7 + rand(1, score/30)
        speed = 15 + (int) (rand.nextDouble()*score/15);

        if(speed>40)speed = 40;
        Bitmap[] image = new Bitmap[numFrames];
        spritesheet = res;
        //petla ktora przechodzi po kazdej bombie z obrazka wtorzac animacje
        for (int i=0; i<image.length; i++)
        {
            image[i]= Bitmap.createBitmap(spritesheet, 0, i*height, width, height);
        }
        animation.setFrames(image);
        //jezeli bomba jest szybsza trzeba wyrównac tempo
        animation.setDelay(100-speed);

    }
    public void update(){
        x-=speed;
        animation.update();
    }
    public  void draw (Canvas canvas){
        try{
            canvas.drawBitmap(animation.getImage(),x, y, null);
        }catch (Exception e){

        }
    }
    //trzeba nadpisac metode getWidth dla obiektu super
    @Override
    public int getWidth()
    {
        //dla wiekszej realnosci deklaracji kolizji
        return width-10;

    }
}
