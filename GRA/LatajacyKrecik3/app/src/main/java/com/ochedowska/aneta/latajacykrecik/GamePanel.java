package com.ochedowska.aneta.latajacykrecik;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.BitmapFactory;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Created by Aneta on 2016-01-14.
 */
//SurfaceView zapewnia powierzchnie rysunku osadzonego wewnatrz hierarchii widoku
public class GamePanel extends SurfaceView implements SurfaceHolder.Callback {

    public static final int WIDTH = 856;
    public static final int HEIGHT = 480;
    public static int MOVESPEED = -5;

    private MainThread thread;
    private Backgroud backgroud;
    private long smokeStartTimer;
    private long missleStartTime;

    private Player player;
    private ArrayList<Smokepuff> smoke;
    private ArrayList<Missile> missiles;

    private ArrayList<TopBorder> topborder;
    private ArrayList<BotBorder> botborder;
    private int maxBorderHeight;
    private int minBorderHeight;

    //dodawanie obramowania az ekran poczatkowy jest zapełniony
    private int progressDenom = 20;
    private boolean topDown = true;
    private boolean botDown = true;
    private boolean newGameCreated;


    private Explosion explosion;
    private long startReset;
    private boolean reset;
    private boolean dissapear;
    private boolean started;
    private int best;
    ZarzadcaBazy zb;




    private Random random = new Random();

    public GamePanel(Context context) {

        // konstruktor klasy SurfaceView
        super(context);
        //dodaje callback do SurfaceView zeby przechwycic wydarzenie
        getHolder().addCallback(this);
        zb = new ZarzadcaBazy(context);
//
//        Log.d("przed k", "tabela");
//        Cursor k = zb.dajWszystkie();
//        Log.d("po k", "tabela");
//
//        while(k.moveToNext()) {
//            int nr = k.getInt(0);
//            String best = k.getString(1);
//            String w =  nr + " " + best;
//            Log.d("rekordy", w);
//        }
        Iterator var5 = zb.theBest().iterator();

        while(var5.hasNext()) {
            Score ds = (Score)var5.next();
            String thebest = ds.getBest();
            best = Integer.parseInt(thebest);
            Log.d("best:",thebest);
        }

        //pozwalam na to aby byl pojemnikiem do przetrzymywania
        //na przetrzymywanie wydarzen
        setFocusable(true);
    }


    //musze tez nadpisac niektore metody z klasy SurfaceView
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //konczymy watek
        boolean retry = true;
        int counter = 0;
        while (retry && counter < 1000) {
            counter++;//upewniam sie ze pentla zostala wykonana dokladnie 1000
            try {
                thread.setRunnig(false);
                thread.join();
                retry = false;
                //zeby garbage collector mogl podniesc objekt
                thread = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //BitmapFactory tworzy obiekty Bitmap z różnych źródeł w tym plików i tablic
        // w tym wypadku tworzy z pliku
        backgroud = new Backgroud(BitmapFactory.decodeResource(getResources(), R.drawable.grassbg1));
        player = new Player(BitmapFactory.decodeResource(getResources(), R.drawable.helicopter), 65, 25, 3);

        topborder = new ArrayList<TopBorder>();
        botborder = new ArrayList<BotBorder>();

        smoke = new ArrayList<Smokepuff>();
        smokeStartTimer = System.nanoTime();

        missiles = new ArrayList<Missile>();
        missleStartTime = System.nanoTime();

        thread = new MainThread(getHolder(), this);
        //tu mozemy rozpoczac petle gry z MainThred
        thread.setRunnig(true);
        thread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (!player.getPlaying() && newGameCreated && reset) {
                player.setPlaying(true);
                player.setUp(true);
            }
            if (player.getPlaying()) {

                if (!started) started = true;
                reset = false;
                player.setUp(true);
            }
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            player.setUp(false);
            return true;
        }

        return super.onTouchEvent(event);
    }


    public void update() {

        if (player.getPlaying()) {
            backgroud.update();
            player.update();

            // Obliczenia progu wysokości granicy może być oparta o wynik
            // Max i min serca granicy są aktualizowane, a granica zmienia kierunek , gdy albo max lub
            // Min sie spotkaja

            maxBorderHeight = 30 + player.getScore() / progressDenom;
            //maksymalna wysokość granicy tak, że granice mogą jedynie byc w sumie 1/2 ekranu
            if (maxBorderHeight > HEIGHT / 4) maxBorderHeight = HEIGHT / 4;
            minBorderHeight = 5 + player.getScore() / progressDenom;

            //check bottom border collision
            for (int i = 0; i < botborder.size(); i++) {
                if (collision(botborder.get(i), player))
                    player.setPlaying(false);
            }

            //check top border collision
            for (int i = 0; i < topborder.size(); i++) {
                if (collision(topborder.get(i), player))
                    player.setPlaying(false);
            }

            //update top border
            this.updateTopBorder();

            //udpate bottom border
            this.updateBottomBorder();


            //dodaje torpedy do timera
            long missileElapsed = (System.nanoTime() - missleStartTime) / 1000000;
            // kiedy punkty beda rosły szybkosc torped również
            if (missileElapsed > (2000 - player.getScore() / 4)) {
                //pierwsza torpeda zaczyna zawsze na srodku
                if (missiles.size() == 0) {
                    missiles.add(new Missile(BitmapFactory.decodeResource(getResources(), R.drawable.missile),
                            WIDTH + 10, HEIGHT / 2, 45, 15, player.getScore(), 13));
                } else {
                    missiles.add(new Missile(BitmapFactory.decodeResource(getResources(), R.drawable.missile), WIDTH + 10,
                            (int) (random.nextDouble() * (HEIGHT - (maxBorderHeight * 2)) + maxBorderHeight), 45, 15, player.getScore(), 13));
                }
                //restartuje timer
                missleStartTime = System.nanoTime();
            }
            //petla ktora działa dla kazdej torpedy
            //update torped
            //sprawdza kolizje
            //usuwa torpede jezeli poza plansza
            for (int i = 0; i < missiles.size(); i++) {
                missiles.get(i).update();
                if (collision(missiles.get(i), player)) {
                    missiles.remove(i);
                    player.setPlaying(false);
                }
                //jezeli torpeda poza plansza
                if (missiles.get(i).getX() < -100) {
                    missiles.remove(i);
                    break;
                }
            }

            long elapsed = (System.nanoTime() - smokeStartTimer) / 1000000;
            if (elapsed > 120) {
                smoke.add(new Smokepuff(player.getX(), player.getY() + 10));
                smokeStartTimer = System.nanoTime();
            }
            //iteruje przez dymowe kupki:P
            for (int i = 0; i < smoke.size(); i++) {
                smoke.get(i).update();
                //jezeli poza ekranem
                if (smoke.get(i).getX() < -10) {
                    smoke.remove(i);
                }
            }
        } else {
            player.resetDY();
            if (!reset) {
                newGameCreated = false;
                startReset = System.nanoTime();
                reset = true;
                dissapear = true;
                explosion = new Explosion(BitmapFactory.decodeResource(getResources(), R.drawable.explosion), player.getX(),
                        player.getY() - 30, 100, 100, 25);
            }

            explosion.update();
            long resetElapsed = (System.nanoTime() - startReset) / 1000000;

            if (resetElapsed > 2500 && !newGameCreated) {
                newGame();
            }


        }
    }

    public boolean collision(GameObject a, GameObject b) {
        if (Rect.intersects(a.getRectangle(), b.getRectangle())) {
            if (player.getScore() > best) {
                Log.d("duzy", "best");

                best = player.getScore();
                String Rec = Integer.toString(best);
                zb.addScore(Rec);
            }

            return true;
        }
        return false;
    }

    @Override
    public void draw(Canvas canvas) {
        final float scaleFactorX = getWidth() / (WIDTH * 1.f);
        final float scaleFactorY = getHeight() / (HEIGHT * 1.f);

        if (canvas != null) {
            final int savedState = canvas.save();
            canvas.scale(scaleFactorX, scaleFactorY);
            backgroud.draw(canvas);
            player.draw(canvas);

            //rysuje dymkowe kupki:P
            for (Smokepuff sp : smoke) {
                sp.draw(canvas);
            }
            //rysuje torpedy
            for (Missile m : missiles) {
                m.draw(canvas);
            }


            //rysuje borders


            //draw topborder
            for (TopBorder tb : topborder) {
                tb.draw(canvas);
            }

            //draw botborder
            for (BotBorder bb : botborder) {
                bb.draw(canvas);
            }
            //draw botborder
            for (BotBorder bb : botborder) {
                bb.draw(canvas);
            }
            //draw explosion
            if (started) {
                explosion.draw(canvas);
            }
            drawText(canvas);
            canvas.restoreToCount(savedState);
        }


    }

    public void updateTopBorder() {
        //every 50 points, insert randomly placed top blocks that break the pattern
        if (player.getScore() % 50 == 0) {
            topborder.add(new TopBorder(BitmapFactory.decodeResource(getResources(), R.drawable.brick
            ), topborder.get(topborder.size() - 1).getX() + 20, 0, (int) ((random.nextDouble() * (maxBorderHeight
            )) + 1)));
        }
        for (int i = 0; i < topborder.size(); i++) {
            topborder.get(i).update();
            if (topborder.get(i).getX() < -20) {
                topborder.remove(i);
                //remove element of arraylist, replace it by adding a new one

                //calculate topdown which determines the direction the border is moving (up or down)
                if (topborder.get(topborder.size() - 1).getHeight() >= maxBorderHeight) {
                    topDown = false;
                }
                if (topborder.get(topborder.size() - 1).getHeight() <= minBorderHeight) {
                    topDown = true;
                }
                //new border added will have larger height
                if (topDown) {
                    topborder.add(new TopBorder(BitmapFactory.decodeResource(getResources(),
                            R.drawable.brick), topborder.get(topborder.size() - 1).getX() + 20,
                            0, topborder.get(topborder.size() - 1).getHeight() + 1));
                }
                //new border added wil have smaller height
                else {
                    topborder.add(new TopBorder(BitmapFactory.decodeResource(getResources(),
                            R.drawable.brick), topborder.get(topborder.size() - 1).getX() + 20,
                            0, topborder.get(topborder.size() - 1).getHeight() - 1));
                }

            }
        }

    }

    public void updateBottomBorder() {
        //every 40 points, insert randomly placed bottom blocks that break pattern
        if (player.getScore() % 40 == 0) {
            botborder.add(new BotBorder(BitmapFactory.decodeResource(getResources(), R.drawable.brick),
                    botborder.get(botborder.size() - 1).getX() + 20, (int) ((random.nextDouble()
                    * maxBorderHeight) + (HEIGHT - maxBorderHeight))));
        }

        //update bottom border
        for (int i = 0; i < botborder.size(); i++) {
            botborder.get(i).update();

            //if border is moving off screen, remove it and add a corresponding new one
            if (botborder.get(i).getX() < -20) {
                botborder.remove(i);


                //determine if border will be moving up or down
                if (botborder.get(botborder.size() - 1).getY() <= HEIGHT - maxBorderHeight) {
                    botDown = true;
                }
                if (botborder.get(botborder.size() - 1).getY() >= HEIGHT - minBorderHeight) {
                    botDown = false;
                }

                if (botDown) {
                    botborder.add(new BotBorder(BitmapFactory.decodeResource(getResources(), R.drawable.brick
                    ), botborder.get(botborder.size() - 1).getX() + 20, botborder.get(botborder.size() - 1
                    ).getY() + 1));
                } else {
                    botborder.add(new BotBorder(BitmapFactory.decodeResource(getResources(), R.drawable.brick
                    ), botborder.get(botborder.size() - 1).getX() + 20, botborder.get(botborder.size() - 1
                    ).getY() - 1));
                }
            }
        }
    }


    public void newGame() {
        dissapear = false;

        botborder.clear();
        topborder.clear();

        missiles.clear();
        smoke.clear();

        minBorderHeight = 5;
        maxBorderHeight = 30;

        player.resetDY();
        player.resetScore();
        player.setY(HEIGHT / 2);

//        if (player.getScore() > best) {
//            Log.d("wieszy","sadsas");
//            best = player.getScore();
//        }

        //stworz obramowanie

        //zainicjuj gorna rame
        for (int i = 0; i * 20 < WIDTH + 40; i++) {
            //pierwsza gorna rama
            if (i == 0) {
                topborder.add(new TopBorder(BitmapFactory.decodeResource(getResources(), R.drawable.brick
                ), i * 20, 0, 10));
            } else {
                topborder.add(new TopBorder(BitmapFactory.decodeResource(getResources(), R.drawable.brick
                ), i * 20, 0, topborder.get(i - 1).getHeight() + 1));
            }
        }
        //zainicjuj dolną rame
        for (int i = 0; i * 20 < WIDTH + 40; i++) {
            //first border ever created
            if (i == 0) {
                botborder.add(new BotBorder(BitmapFactory.decodeResource(getResources(), R.drawable.brick)
                        , i * 20, HEIGHT - minBorderHeight));
            }
            //dodawaj ramy az ekran poczatkowy bedzie pełny
            else {
                botborder.add(new BotBorder(BitmapFactory.decodeResource(getResources(), R.drawable.brick),
                        i * 20, botborder.get(i - 1).getY() - 1));
            }
        }

        newGameCreated = true;


    }

    public void drawText(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(30);
        //tu zmienialam score
        int yourscore = player.getScore();


        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        canvas.drawText("DISTANCE: " + (player.getScore() * 3), 10, HEIGHT - 10, paint);
        canvas.drawText("BEST: " + best, WIDTH - 215, HEIGHT - 10, paint);
        canvas.drawText("SCORE: " + yourscore, WIDTH - 400, HEIGHT - 10, paint);


        if (!player.getPlaying() && newGameCreated && reset) {
            Paint paint1 = new Paint();
            paint1.setTextSize(40);
            paint1.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
            canvas.drawText("PRESS TO START", WIDTH / 2 - 50, HEIGHT / 2, paint1);

            paint1.setTextSize(20);
            canvas.drawText("PRESS AND HOLD TO GO UP", WIDTH / 2 - 50, HEIGHT / 2 + 20, paint1);
            canvas.drawText("RELEASE TO GO DOWN", WIDTH / 2 - 50, HEIGHT / 2 + 40, paint1);
        }
    }
}
